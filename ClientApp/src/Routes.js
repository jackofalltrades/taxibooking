import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";

import TaxiBookingList from "./components/screens/TaxiBooking/TaxiBookingList";
import NewTaxiBooking from "./components/screens/TaxiBooking/NewTaxiBooking";


const Routes = () => {
    return (
        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={TaxiBookingList} />
                <Route path="/list" exact component={TaxiBookingList} />
                <Route path="/newtaxibooking" exact component={NewTaxiBooking} />
            </Switch>
        </BrowserRouter>
    );
}

export default Routes;
