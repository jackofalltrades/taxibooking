import React, { useState, useEffect } from 'react';
import Layout from "../../core/Layout";
import BookingList from "./components/BookingList";
import { NoDataFound, TrendTitleButton } from '../../shared/SharedComponent';
import { getBookingList, updateBookStatus } from '../../../services/dataservice/TaxiDataservice';

export default function LeadsDashboard(props) {

    const [listData, setListData] = useState([]);

    useEffect(() => {
        loadResource();
    }, []);

    const loadResource = async () => {
        await getBookingList().then((response) => {
            debugger;
            console.log("Calling Response ---------");
            console.log(response);
            if (response.objList !== undefined && response.objList !== null) {
                setListData(response.objList);
            }
            else
                setListData([]);
        });        
    }

    const updateStatus = async (bookId, statusId) => {
        await updateBookStatus(bookId, statusId).then((response) => {
            debugger;
            console.log("Calling Response ---------");
            console.log(response);
            if (response.isSuccess === true) {
                loadResource();
            }
            else
                alert("Something went wrong");
        });
    }

    return (
        <Layout title="Booking List">
            <div class="container-fluid">
                <div >
                    <div className="teamuser_row">                        
                        <TrendTitleButton Title="New Booking" Link="/newtaxibooking" history={props.history} />
                    </div>
                </div>

                <div class="row">
                    <BookingList listData={listData} updateStatus={updateStatus} />
                </div>
            </div>
        </Layout>
    )
};