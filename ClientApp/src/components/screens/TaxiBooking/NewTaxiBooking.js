import React, { useState, useEffect } from 'react';
import Layout from "../../core/Layout";
import BookingList from "./components/BookingList";
//import TeamUsers from "./../CallingViability/components/TeamUsers";

import { NoDataFound, TrendTitleButton } from '../../shared/SharedComponent';
import { getBookingResource, getAvailableTaxi, bookTaxi } from '../../../services/dataservice/TaxiDataservice';
import Table from 'react-bootstrap/Table';

export default function NewTaxiBooking(props) {

    const [bookingMod, setBookingMod] = useState({});
    const [taxiList, setTaxiList] = useState([]);

    useEffect(() => {
        //loadResource();
    });

    //const loadResource = async () => {
    //    await getBookingResource().then((response) => {
    //        debugger;
    //        console.log("Calling Response ---------");
    //        console.log(response);
    //        if (response.objTaxiList !== undefined) {
    //            setListData(response.objList);
    //        }
    //    });        
    //}

    const checkAvailableTaxi = async () => {        
        if (bookingMod.CusLatitude !== undefined && bookingMod.CusLongitude !== undefined) {
            await getAvailableTaxi(bookingMod.CusLatitude, bookingMod.CusLongitude).then((response) => {                
                if (response.objList !== undefined && response.objList !== null) {
                    setTaxiList(response.objList);
                }
                else
                    setTaxiList([]);
            });
        }
        else {
            alert("Please enter valid details and try again");
        }
    }

    const _bookTaxi = async (taxiId) => {
        if (bookingMod.CusLatitude !== undefined && bookingMod.CusLongitude !== undefined
            && bookingMod.CusName !== undefined && bookingMod.CusContact !== undefined) {
            //let objBook = bookingMod;
            bookingMod.TaxiId = taxiId;
            await bookTaxi(bookingMod).then((response) => {
                console.log("Calling Response ---------");
                console.log(response);
                if (response.isSuccess === true) {
                    props.history.push("/list");
                }
                else {
                    alert("Something went wrong. Please try again later.");
                }
            });
        }
        else {
            alert("Please enter all the valid details and try again");
        }
    }

    return (
        <Layout title="New Booking">
            <div class="container-fluid">
                {/* <div >
                    <div className="teamuser_row">                        
                        <TrendTitleButton Title="New Booking" Link="/booktaxi" history={props.history} />
                    </div>
                </div> */}

                <div >
                    <div className="form-row">
                        <div className="form-group col-6 col-md-2">
                            <strong className="text-muted d-block mb-2">
                                Customer Name
                        </strong>
                            <input type="text" className="form-control" value={bookingMod.CusName} onChange={(e) => setBookingMod({ ...bookingMod,  CusName : e.target.value})} />
                        </div>
                        <div className="form-group col-6 col-md-2">                            
                            <strong className="text-muted d-block mb-2">
                                Customer Contact
                        </strong>
                            <input type="text" className="form-control" value={bookingMod.CusContact} onChange={(e) => setBookingMod({ ...bookingMod,  CusContact: e.target.value })} />
                            
                        </div>
                        <div className="form-group col-6 col-md-2">
                            <strong className="text-muted d-block mb-2">
                                Customer Latitude
                        </strong>
                            <input type="number" min="-90" max="90" className="form-control" value={bookingMod.CusLatitude} onChange={(e) => setBookingMod({ ...bookingMod,  CusLatitude: e.target.value })} />
                        </div>   
                        <div className="form-group col-6 col-md-2">
                            <strong className="text-muted d-block mb-2">
                                Customer Longitude
                        </strong>
                            <input type="number" className="form-control" value={bookingMod.CusLongitude} onChange={(e) => setBookingMod({ ...bookingMod,  CusLongitude: e.target.value })}  />
                        </div>   
                        <div className="form-group col-3 col-md-2">                            
                            <strong className="text-muted d-block mb-2">
                                &nbsp;
                        </strong>
                            <input type="button" className="form-control btn-primary" onClick={() => checkAvailableTaxi()} value="Check" />
                        </div>
                        <div className="form-group col-3 col-md-2">
                            <strong className="text-muted d-block mb-2">
                                &nbsp;
                        </strong>
                            <input type="button" className="form-control" onClick={() => { props.history.push("/") }} value="Back to list" />
                        </div>
                    </div>
                </div>
                <div>
                    <Table striped bordered hover >
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Taxi Number</th>
                                <th>Taxi Location</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {taxiList.map((itm, key) => (
                                <tr>
                                    <td>{key + 1}</td>
                                    <td>{itm.TaxiName}</td>
                                    <td>{itm.CurLatitude + ", " + itm.CurLongitude}</td>
                                    <td><input type="button" value="Book" className="btn btn-primary" onClick={() => _bookTaxi(itm.TaxiId)} /></td>
                                </tr>
                            ))}                            
                        </tbody>
                    </Table>
                    </div>
            </div>

        </Layout>
    )
};