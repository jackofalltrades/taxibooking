﻿import React from 'react';
import Card from 'react-bootstrap/Card';
import { CardTitle, formatDate } from "../../../shared/SharedComponent";
import Table from 'react-bootstrap/Table';

export default function BookingList(props) {
    let objlist = props.listData;

    const _updateStatus = (bookId, statusId) => {
        props.updateStatus(bookId, statusId);
    }

    return (
        <div >
            <Table striped bordered hover >
                <thead>
                    <tr>
                        <th>Booking Id</th>
                        <th>Date</th>
                        <th>Customer Name</th>
                        <th>Customer Contact</th>
                        <th>Taxi</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {objlist.map((itm, key) => {
                        return (
                            <tr>
                                <td>{itm.BookId}</td>
                                <td>{formatDate(itm.BookDate, "DD/MM/yyyy")}</td>
                                <td>{itm.CusName}</td>
                                <td>{itm.CusContact}</td>
                                <td>{itm.TaxiName}</td>
                                <td>{itm.StatusName}</td>
                                <td> {itm.StatusId === 1 ?
                                    <div>
                                        <input type="button" className="btn btn-warning" value="End Trip" onClick={() => { _updateStatus(itm.BookId, 4)}} />
                                        <input type="button" className="btn btn-danger" value="Cancel Trip" onClick={() => { _updateStatus(itm.BookId, 3) }} />
                                    </div>
                                    : ""}</td>
                            </tr>
                        );
                    }) 
                    }
                </tbody>
            </Table>
        </div>
    )
};