import React, { useState, useRef } from "react";
// import { slide as Menu } from "react-burger-menu";
import profile from "../../assets/images/profile.png";
import Burger from "./Burger";
import SideMenu from "./SideMenu";
import profImg from "../../assets/images/profile.png";
const Menu = ({ title }) => {
    const [open, setOpen] = React.useState(false);
    // const node = React.useRef();
    let data = JSON.parse(localStorage.getItem("jwt"));
    let fname = data.data.user.first_name;
    let lname = data.data.user.last_name;
    let tname = data.data.team.name;
    return (
        <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
            <div class="container-fluid">
                <div class="navbar-wrapper">
                    <h3>{title}</h3>
                    {/* <a class="navbar-brand" href="javascript:;">{title}</a> */}
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                    <span class="navbar-toggler-icon icon-bar"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end">
                    {/* <form class="navbar-form">
                        <div class="input-group no-border">
                            <input type="text" value="" class="form-control" placeholder="Search..." />
                            <button type="submit" class="btn btn-white btn-round btn-just-icon">
                                <i class="material-icons">search</i>
                                <div class="ripple-container"></div>
                            </button>
                        </div>
                    </form> */}
                    <ul class="navbar-nav">                        
                        <li class="nav-item dropdown">
                            <strong>{fname + " " + lname} <br /> {tname}</strong>
                            
                        </li>                        
                        <li class="nav-item dropdown">
                            <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {/* <i class="material-icons">person</i> */}
                                <img src={profImg} style={{width: 50}} />
                                <p class="d-lg-none d-md-block">
                                    Account
                  </p>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                                {/* <a class="dropdown-item" href="#">Profile</a>
                                <a class="dropdown-item" href="#">Settings</a>
                                <div class="dropdown-divider"></div> */}
                                <a class="dropdown-item" href="#">Log out</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <i class="material-icons" onClick={() => {
                                localStorage.removeItem("jwt");
                                localStorage.removeItem("team");
                                window.location.href = "/";
                            }} style={{ cursor:'pointer' }}>logout</i>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
    );
};
export default Menu;