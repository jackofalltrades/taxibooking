import React from "react";
import { Link } from "react-router-dom";
//import { PageTitle } from "../shared/StaticData";

const SideMenu = ({ open, title, history }) => {
    return (
        <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
            <div class="logo">

                <a href="http://www.creative-tim.com" class="simple-text logo-normal">
                    {/* <img src={LogoImg} style={{ width: 100 }} />                    */}
                    <h2 style={{ color: "white" }}>Taxi Booking</h2>
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li class="nav-item active">
                        <Link class="nav-link" to="/kpi">
                            <i class="material-icons">mode_standby</i>
                            <p>Taxi Booking</p>
                        </Link>
                    </li>

                </ul>
            </div>
        </div>
    );
};
export default SideMenu;
