import React, { useState, useEffect } from "react";
import Layout from "./Layout";
import FileExplorer from "../organizationAdmin/FileExplorer";

const Home = () => {
  return (
    <Layout title="SalesGo Home Page" description="SalesGo Home Page">
      <h2>Home Page</h2>
      <FileExplorer />
    </Layout>
  );
};

export default Home;
