import React from 'react';
//import Badge from 'react-bootstrap/Badge';
//import Button from 'react-bootstrap/Button';
import { Badge, Button, ToggleButton, ToggleButtonGroup, Alert } from 'react-bootstrap';
import moment from 'moment';
import { Line } from 'recharts';
import { Link } from "react-router-dom";

export function CardTitle(props) {
    debugger;
    let title = props.title;
    //let value = props.value;
    let icon = props.icon;

    if (icon === "") {
        return (
            <div class="card-icon" style={{ marginRight: 0, background: 'lightgray', boxShadow: '0.5', border: "1px solid #104377" }}>
                <h6 class="cardTitleColor">{title}</h6>
                {/* <i class="material-icons">content_copy</i> */}
            </div>
        )
    }
    else
        return (
            <div class="card-icon">
                <i class="material-icons">{icon}</i>
                {/* <i class="material-icons">content_copy</i> */}
            </div>
        )
};

export function YearSelection(props) {
    let filterSel = props.filterSel;
    return (
        <div>            
            {/*<Badge variant={filterSel === "YTD" ? "danger" : "warning"} onClick={() => props.selectYear("YTD")}>YTD</Badge>{' '}
            <Badge variant={filterSel === "QTD" ? "danger" : "warning"} onClick={() => props.selectYear("QTD")}>QTD</Badge>{' '}
            <Badge variant={filterSel === "MTD" ? "danger" : "warning"} onClick={() => props.selectYear("MTD")}>MTD</Badge>{' '}
            <Badge variant={filterSel === "WTD" ? "danger" : "warning"} onClick={() => props.selectYear("WTD")}>WTD</Badge> */}
            {/* <input type="text" className="form-control" value="" placeholder="Search by name" /> */}
            <Button variant={filterSel === "YTD" ? "info" : "outline-dark"} style={{ backgroundColor: filterSel === "YTD" ? "#104377" : ""}} onClick={() => props.selectYear("YTD")}>YTD</Button>{' '}
            <Button variant={filterSel === "QTD" ? "info" : "outline-dark"} style={{ backgroundColor: filterSel === "QTD" ? "#104377" : "" }} onClick={() => props.selectYear("QTD")}>QTD</Button>{' '}
            <Button variant={filterSel === "MTD" ? "info" : "outline-dark"} style={{ backgroundColor: filterSel === "MTD" ? "#104377" : "" }} onClick={() => props.selectYear("MTD")}>MTD</Button>{' '}
            <Button variant={filterSel === "WTD" ? "info" : "outline-dark"} style={{ backgroundColor: filterSel === "WTD" ? "#104377" : "" }} onClick={() => props.selectYear("WTD")}>WTD</Button>
        </div>
    );
};

export function LegandSelection(props) {
    let dat = props.objData;
    let legandSel = props.legandSel;

    return (
        <div>
            {/* <label>Search Legand123</label><br /> }            
            <Badge variant={legandSel === "A" ? "danger" : "warning"} style={{ marginLeft: 10 }}
                onClick={() => props.selectLegand("A")}>All</Badge>
            {dat.map((leg, i) => (
                <Badge variant={leg.code === legandSel ? "danger" : "warning"} key={i} style={{marginLeft : 10}}
                    onClick={() => props.selectLegand(leg.code)}>{leg.name }</Badge>
            ))
            */}
            <ToggleButtonGroup type="radio" value={legandSel} onChange={(e) => alert(e.target.value)} name="togbut">
                <ToggleButton variant="blue" value={"A"} key="a" style={{ "background-color": legandSel === "A" ? '#104377' : '', color: legandSel === "A" ? '#f8f9fa' : 'black' }}
                    onClick={() => props.selectLegand("A")}>All</ToggleButton>
            {dat.map((leg, i) => (
                <ToggleButton variant="blue" key={i} style={{ marginLeft: 10, "background-color": leg.code === legandSel ? '#104377' : '', color: leg.code === legandSel ? '#f8f9fa' : 'black' }}
                    value={leg.code}
                    onClick={() => props.selectLegand(leg.code)}>{leg.name}</ToggleButton>
            ))
                }
                </ToggleButtonGroup>
        </div>
    );
};

export function formatDate(dateString, dateFormat) {
    //console.log("format : " + dateString);
    const parsed = moment(new Date(dateString));

    if (!parsed.isValid()) {
        return dateString;
    }
    
    //return parsed.format('DD/MM/YYYY');
    return parsed.format(dateFormat);
}

export function formatGraphDate(objdate, objfor) {
    let cformat = objfor === "QTD" ? "MMMM yy" : objfor === "YTD" ? "MMMM yy" : "DD MMMM";
    return formatDate(objdate, cformat); // "MMMM yy");
}

export function NoDataFound() {
    return (
        //<Alert variant="danger">No Data found</Alert>
        <p>No Data found</p>
    );
}

export function CountValueType(props) {
    return (
        <div>
            <strong className="text-muted d-block mb-2">
                Count / Value Type
                          </strong>
            {/* <select
                name="selectedTeam_4"
                value={props.val}
                onChange={(e) => {
                    props.valChange(e.target.value);
                }}
                className="form-control"
            >
                <option value="0">Select</option>
                <option value="V">Value</option>
                <option value="C">Count</option>
            </select> */}

            <ToggleButtonGroup type="radio" value={props.val} name="togbut">
                <ToggleButton variant="blue" value={"V"}
                    style={{ "background-color": props.val === "V" ? '#104377' : '', color: props.val === "V" ? '#f8f9fa' : 'black' }}
                    onClick={(e) => props.valChange(e.target.value)}>Value</ToggleButton>               
                <ToggleButton variant="blue" value={"C"}
                    style={{ "background-color": props.val === "C" ? '#104377' : '', color: props.val === "C" ? '#f8f9fa' : 'black' }}
                    onClick={(e) => props.valChange(e.target.value)}>Count</ToggleButton>
            </ToggleButtonGroup>
        </div>
        )
};

export function getUserTeam() {
    var team = localStorage.getItem("team");
    team = team === undefined ? "1" : team;
    return team;
}

export function LineSection(props) {
    debugger;
    return (
        <Line type="monotone" dataKey="viable" stroke="#040EFB" strokeWidth={2} dot={false} />
    );
};

export const ChartConfig = {
    LineType: "monotone",
    isDot: false,
    StrokeWidth: 2,

    BarType: "category",
    BarSize: 20,
    BarColor: "#0491FB"
}

export function TrendTitleButton(props) {
    return (
        <div>
            <Button variant="Info" className="Btn_blue" onClick={() => props.history.push(props.Link)} >
                <i class="material-icons">add</i>{' '}
                {props.Title}</Button>
        </div>
    );
};
