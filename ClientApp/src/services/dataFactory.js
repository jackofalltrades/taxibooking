import axios from "axios";
const host = "https://localhost:44331/api/"; // "http://15.206.216.97/";

export async function GetApiData(urlCode) {
    return await //trackPromise(
        axios
            .get(host + "/" + urlCode, {
                headers: {
                    "Content-Type": "application/JSON"                    
                },
            })
            .then((response) => {
                return response;
            })
            .then((responseJson) => {
                //debugger;
                return responseJson.data;
            })
            .catch((error) => {
                //debugger;
                return null;
            });
    //);
}

export async function PostApiData(urlCode, objData) {    

    return await
        //fetch(host + "/" + urlCode, {
        //    mode: 'no-cors',
        //    method: 'post',
        //    headers: {
        //        //'Accept': 'application/json, text/plain, */*',
        //        'Content-Type': 'application/json'
        //    },
        //    body: JSON.stringify(objData)
        //})
        //axios.post(host + "/" + urlCode, objData, {
        //    //mode: 'no-cors',
        //    headers: {
        //        //"Content-Type": "application/JSON"
        //        'Accept': 'application/json',
        //        //'Content-Type': 'application/json',
        //        'Content-Type': 'application/json;charset=UTF-8',
        //        "Access-Control-Allow-Origin": "*",
        //    },
        //})

        axios({
        method: "POST",
        url: host + "/" + urlCode,
            data: objData,
            //withCredentials: false,
        headers: {
            "Content-Type": "application/json",
            //"Access-Control-Allow-Origin": "*"
        },
    })
        .then(function (response) {
            //debugger;
            return response.data;            
        })
        .catch(function (error) {
            //debugger;
            //handle error
            let objError = {
                status: "e",
                //data: error.response.data,
            };

            return null; //objError;
        });
}

export async function PostFetch(ctrl, objData) {
    var token = await localStorage.getItem("token");

    return fetch(host + "/" + ctrl, {
        method: 'POST',
        body: JSON.stringify(objData),
        headers: new Headers({
            'Content-Type': 'application/JSON',
            'Authorization': 'Bearer ' + token
        })
    })
        .then((response) => response.json())
        .then((responseJson) => {
            console.log("p");
            //console.log(responseJson);
            return responseJson;
        })
        .catch((error) => {
            return null;
        });
};