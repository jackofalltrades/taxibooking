import { GetApiData, PostApiData, PostFetch } from "../../services/dataFactory";
import { getUserTeam } from "./../../components/shared/SharedComponent"; // "../shared/SharedComponent";

const bookingListApi = "taxibooking/getbookinglist";
const newBookingApi = "taxibooking/getbookingresource";
const availableTaxiApi = "taxibooking/GetAvailableTaxi";
const bookTaxiApi = "taxibooking/BookTaxi";
const updateBookStatusAPI = "taxibooking/UpdateBookStatus";

export const getBookingList = async () => {
    //debugger;
    var objData = await GetApiData(bookingListApi).then((response) => {
        //debugger;
        return response;
    });

    return objData;
}

export const getAvailableTaxi = async (lat, lng) => {
    var objData = await GetApiData(availableTaxiApi + "/" + lat + "/" + lng +"/").then((response) => {
        //debugger;
        return response;
    });

    return objData;
}

export const bookTaxi = async (objDat) => {
    let booktaxiurl = bookTaxiApi + "/" + objDat.CusName + "/" + objDat.CusContact + "/" + objDat.CusLatitude + "/" + objDat.CusLatitude + "/" + objDat.TaxiId;
    var objData = await GetApiData(booktaxiurl).then((response) => {
        //debugger;
        return response;
    });

    //var objData = await PostApiData(bookTaxiApi, objDat).then((response) => {
    //    //debugger;
    //    return response;
    //});

    return objData;
}

export const updateBookStatus = async (bookId, statusId) => {
    let updateBookUrl = updateBookStatusAPI + "/" + bookId + "/" + statusId;
    var objData = await GetApiData(updateBookUrl).then((response) => {
        //debugger;
        return response;
    });

    return objData;
}