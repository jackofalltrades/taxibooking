﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web;

namespace TaxiBooking.Models.TaxiBooking
{
    public class MTaxi
    {
        public int TaxiId { get; set; }
        public string TaxiName { get; set; }
        public decimal? CurLatitude { get; set; }
        public decimal? CurLongitude { get; set; }

        public GeoCoordinate Coordinates { get; set; }
    }
}