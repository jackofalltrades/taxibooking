﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxiBooking.Models.TaxiBooking
{
    public class MTaxiBooking
    {
        public int BookId { get; set; }
        public DateTime BookDate { get; set; }
        public string CusName { get; set; }
        public string CusContact { get; set; }
        public int TaxiId { get; set; }
        public decimal? CusLatitude { get; set; }
        public decimal? CusLongitude { get; set; }
        public int StatusId { get; set; }

        public string TaxiName { get; set; }
        public string StatusName { get; set; }
    }
}