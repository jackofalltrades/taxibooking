﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaxiBooking.Models.TaxiBooking
{
    public class MStatus
    {
        public int StatusId { get; set; }
        public string StatusName { get; set; }
    }
}