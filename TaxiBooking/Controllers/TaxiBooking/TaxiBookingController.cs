﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using TaxiBooking.Models.TaxiBooking;
using TaxiBooking.Service;

namespace TaxiBooking.Controllers.TaxiBooking
{
    public class TaxiBookingController : ApiController
    {
        [HttpGet]
        [Route("api/TaxiBooking/GetBookingList")]
        public HttpResponseMessage GetBookingList()
        {
            try
            {
                TaxiBookingDataService objTaxiDataService = new TaxiBookingDataService();
                var res = new
                {
                    objList = objTaxiDataService.getBookingList()
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [EnableCors(origins: "http://localhost:3000/", headers: "*", methods: "*")]
        [HttpGet]
        [Route("api/TaxiBooking/GetBookingResource")]
        public HttpResponseMessage GetBookingResource()
        {
			try
			{
                TaxiDataService objTaxiDataService = new TaxiDataService();
                var res = new
                {
                    objTaxiList = objTaxiDataService.getTaxiList()
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
			catch (Exception ex)
			{
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
			}
        }

        [EnableCors(origins: "http://localhost:3000/", headers: "*", methods: "*")]
        [HttpGet]
        [Route("api/TaxiBooking/GetAvailableTaxi/{lat}/{lng}")]
        public HttpResponseMessage GetAvailableTaxi(double lat, double lng)
        {
            try
            {
                TaxiBookingDataService objTaxiDataService = new TaxiBookingDataService();
                var res = new
                {
                    objList = objTaxiDataService.getAvailableTaxi(lat, lng)
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        
        [HttpGet]
        [Route("api/TaxiBooking/BookTaxi/{cusName}/{cusContact}/{lat}/{lng}/{taxiId}")]
        public HttpResponseMessage BookTaxi(string cusName, string cusContact, decimal lat, decimal lng, int taxiId)
        {
            try
            {
                TaxiBookingDataService objTaxiDataService = new TaxiBookingDataService();

                MTaxiBooking objDat = new MTaxiBooking();
                objDat.CusContact = cusContact;
                objDat.CusLatitude = lat;
                objDat.CusLongitude = lng;
                objDat.CusName = cusName;
                objDat.TaxiId = taxiId;

                var isSuccess = objTaxiDataService.createTaxiBooking(objDat);

                var res = new
                {
                    isSuccess
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        [EnableCors(origins: "http://localhost:3000/", headers: "*", methods: "*")]
        [HttpGet]
        [Route("api/TaxiBooking/UpdateBookStatus/{bookId}/{statusId}")]
        public HttpResponseMessage UpdateBookStatus(int bookId, int statusId)
        {
            try
            {
                TaxiBookingDataService objTaxiDataService = new TaxiBookingDataService();
                var isSuccess = objTaxiDataService.updateBookingStatus(bookId, statusId);

                var res = new
                {
                    isSuccess
                };

                return Request.CreateResponse(HttpStatusCode.OK, res);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
            }
        }

        //[EnableCors(origins: "http://localhost:3000/", headers: "*", methods: "*")]
        //[HttpPost]
        //[Route("api/TaxiBooking/BookTaxis")]
        //public HttpResponseMessage BookTaxis([FromBody] MTaxiBooking objDat)
        //{
        //    try
        //    {
        //        TaxiBookingDataService objTaxiDataService = new TaxiBookingDataService();

        //        var isSuccess = objTaxiDataService.createTaxiBooking(objDat);

        //        var res = new
        //        {
        //            isSuccess = isSuccess
        //        };

        //        return Request.CreateResponse(HttpStatusCode.OK, res);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Request.CreateResponse(HttpStatusCode.InternalServerError, ex.Message);
        //    }
        //}
    }
}
