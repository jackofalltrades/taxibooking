﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Linq;
using System.Web;
using TaxiBooking.Models.DB;
using TaxiBooking.Models.TaxiBooking;

namespace TaxiBooking.Service
{
    public class TaxiBookingDataService
    {        
        TaxiDBEntities objEF = new TaxiDBEntities();

        public List<MTaxiBooking> getBookingList()
        {
            var bookList = objEF.MS_TAXI_BOOKING.Select(s => new MTaxiBooking
            {
                BookDate = s.BOOK_DATE,
                BookId = s.BOOK_ID,
                CusContact = s.CUS_CONTACT,
                CusName = s.CUS_NAME,
                StatusName = s.MS_MAST_BOOKING_STATUS.STATUS_NAME,
                TaxiName = s.MS_MAST_TAXI.TAXI_NAME,
                StatusId = s.STATUS_ID
            }).OrderByDescending(s => s.BookId).ToList();
            return bookList;
        }

        public List<MTaxi> getAvailableTaxi(double lat, double lgt)
        {
            try
            {
                var coord = new GeoCoordinate(lat, lgt);
                var taxiList = objEF.MS_MAST_TAXI
                    .Where(s => s.IS_ACTIVE == true && s.MS_TAXI_BOOKING.FirstOrDefault(c => c.STATUS_ID == 1) == null)
                    .Select(s => new MTaxi{
                        TaxiId = s.TAXI_ID,
                        TaxiName = s.TAXI_NAME,
                        CurLatitude = s.CURRENT_LATITUDE,
                        CurLongitude = s.CURRENT_LONGITUDE
                       //Coordinates = new GeoCoordinate(Convert.ToDouble(s.CURRENT_LATITUDE), Convert.ToDouble(s.CURRENT_LONGITUDE))
                    })
                    //.OrderBy(s => s.Coordinates.GetDistanceTo(coord))
                    .ToList();

                foreach(var itm in taxiList)
                {
                    itm.Coordinates = new GeoCoordinate(Convert.ToDouble(itm.CurLatitude), Convert.ToDouble(itm.CurLongitude));
                }

                taxiList = taxiList.OrderBy(s => s.Coordinates.GetDistanceTo(coord)).ToList();

                //locations.Select(x => new GeoCoordinate(x.Latitude, x.Longitude))
                //   .OrderBy(x => x.GetDistanceTo(coord))
                //   .First();

                return taxiList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool createTaxiBooking(MTaxiBooking objDat)
        {
            try
            {
                MS_TAXI_BOOKING objMod = new MS_TAXI_BOOKING();
                objMod.BOOK_DATE = DateTime.Now;
                objMod.CREATE_DATE = DateTime.Now;
                objMod.CUS_CONTACT = objDat.CusContact;
                objMod.CUS_LATITUDE = objDat.CusLatitude;
                objMod.CUS_LONGITUDE = objDat.CusLongitude;
                objMod.CUS_NAME = objDat.CusName;
                objMod.TAXI_ID = objDat.TaxiId;
                objMod.STATUS_ID = 1;

                objEF.MS_TAXI_BOOKING.Add(objMod);
                objEF.SaveChanges();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool updateBookingStatus(int bookId, int statusId)
        {
            try
            {
                var objBookDetail = objEF.MS_TAXI_BOOKING.FirstOrDefault(s => s.BOOK_ID == bookId);

                if(objBookDetail != null)
                {
                    objBookDetail.STATUS_ID = statusId;

                    objEF.SaveChanges();
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}