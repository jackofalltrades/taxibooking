﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaxiBooking.Models.DB;
using TaxiBooking.Models.TaxiBooking;

namespace TaxiBooking.Service
{
    public class TaxiDataService
    {
        TaxiDBEntities objEF = new TaxiDBEntities();
        
        public List<MTaxiBooking> getTaxiBookingList()
        {
            try
            {
                var objBookingList = objEF.MS_TAXI_BOOKING                    
                    .Select(s => new MTaxiBooking
                    {
                        BookId = s.BOOK_ID,
                        BookDate = s.BOOK_DATE,
                        CusContact = s.CUS_CONTACT,
                        CusName = s.CUS_NAME,
                        CusLatitude = s.CUS_LATITUDE,
                        CusLongitude = s.CUS_LONGITUDE,
                        TaxiId = s.TAXI_ID,
                        TaxiName = s.MS_MAST_TAXI.TAXI_NAME,
                        StatusName = s.MS_MAST_BOOKING_STATUS.STATUS_NAME
                    }).ToList();

                return objBookingList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public List<MTaxi> getTaxiList()
        {
            try
            {
                var objTaxiList = objEF.MS_MAST_TAXI
                    .Where(s => s.IS_ACTIVE == true)
                    .Select(s => new MTaxi
                    {
                        TaxiId = s.TAXI_ID,
                        TaxiName = s.TAXI_NAME,
                        CurLatitude = s.CURRENT_LATITUDE,
                        CurLongitude = s.CURRENT_LONGITUDE
                    }).ToList();

                return objTaxiList;
			}
			catch (Exception ex)
			{
                return null;
			}
        }

        public List<MStatus> getStatusList()
        {
            try
            {
                var objStatusList = objEF.MS_MAST_BOOKING_STATUS
                    .Where(s => s.IS_ACTIVE == true)
                    .Select(s => new MStatus
                    {
                        StatusId = s.STATUS_ID,
                        StatusName = s.STATUS_NAME
                    }).ToList();

                return objStatusList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}